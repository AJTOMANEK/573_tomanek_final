Package

climate_sheets.py will allow users to output graphs of the mean temperature, wind speed, and air pressure over the stations history as well as output the maximum and minimum values of temperature, wind speed, and air pressure over the stations history. 


Setup

The setup file will allow for the import of the module into the users python environment. To import the pakage into your environment use 
the following in your command line: 
    python setup.py install 


Running the Module

Data will be imported using a .csv file of the station climatology data provided from AMRDC. 

The user will need to provide the following to execute the module: 
    - station_name: the user will provide the name of the station which they are accessing to then serve as a marker for downloaded files. 
    - station_input: User will need to provide the .csv file of the station they are wishing to access data from. 
    - save_path: The user will provide the path where they would like the output files saved upon completion of running the package. 


Development

Anastasia Tomanek as part of the AOS 573 class at UW-Madison is a contributor to this package with the help in instructor Hannah Zanowski. ajtomanek@wisc.edu