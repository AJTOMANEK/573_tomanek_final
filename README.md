PROJECT PURPOSE

My internship and and employment at SSEC is through the Antarctic Meteorological Data and Research Center (AMRDC). One of the various tasks I have been armed with is the upkeep of climatology sheets for our various weather stations across Antarctica. Earlier this year upon starting this task, it occured to me that the process for completing this work was redundent and could easily be expidited with the help of a Python code. Over the summer I was not equipped with the knowledge to create such a tool which was a large component in taking this class. 

In our original .xlsx file for each station's climatology, is the main sheet that houses all the data for the station. Subsheets are maintained for mean temperature, mean wind speed, mean air pressure, and max and min values for those variables over the stations history. Data values were copied from the main sheet to these subsheets to then be made into graphs which showed the trends for these variables over the stations history. 

Through the creation of this script, and later development into a package started in this project, it is my hope to automate the graphical data output for all stations so trends and important values can be regularly updated to our new repository that was introduced at our WAMC/YOPP-SH meeting in August of 2022. Interactive data products have also been mentioned as a hope for the repository in further development which can put to use other skills have learned in this class using cartopy and other advanced mapping packages. I also know there are many projects on the backburner as others take priority. By automating this process, time being spent doing the more redundant tasks will be saved so time can get spent working on other projects and asks of the group. 


PROCESS

To access the repository containing elements of the project, please use the following link to "git clone https://git.doit.wisc.edu/AJTOMANEK/573_tomanek_final.git" in your terminal window. 

You may use the AOS573 Jupyter environment to run the code, which is also provided in the .yml file of the repository. 

A folder noted as "working_files" are meerly from my process of development and not necessary for substance towards my final project. 

When working with each "general_df..." file, please change the path in the third cell of code so the products are saved where you want them. Output products are already provided in the folder for each station based on the code ran on my end. 


1. Byrd Station Folder

The development of this package was initiated through the use of the Byrd climatology sheet provided by the AMRDC at the University of Wisconsin-Madison (UW-Madison) and Madison Area Technical College (MATC). 

The initial creation of the script proved some difficulty given the formatting of the .csv file did not have a designated year column, only months. To get around this, I utilized a for loop which find the years in the '% pressure missing' column and assigns that values to each following row in a new year column until it reaches the last year of the data. 

In the Byrd folder is supplied the script in .ipynb format which runs and produces the output graphs for mean temperature, mean wind speed, and mean air pressure in addition to the maximum and minimume values present for each month for the following variables at that station over it's history. These output products are then saved to the named save_path provided at the beginning of the code and names based on the product and station_name given. 

This code also has lines which find the absolute maximum and minimum temperature as historical records for each variable may want to be incorperated in products or identified for specific stations. I made sure I was able to find these values using my dataframe in the event they are used on searches for moving forward. 


2. Testing using Bear Peninsula 

After the creation of the package, it was then tested using data from Bear Peninsula, another station maintained by the AMRDC. It is noted that the exported .csv file format varied from Byrd, which is a common roadblock to run into when working with multiple datasets even with the same intended purpose. Given this obsticle, minor changes had to be made using the .ipynb file provided in the bear_peninsula folder. 

Some of these changes included the slight variation in notation of the heading of columns and two columns, 17 and 18, that were not present in this .csv but were in Byrd's. This did not allow the code to run exactly as written for this station, but allowed me to note some valuable differences which can be adapted in the package script to ensure flexibility for all station climatology sheets. 

After moving this station's folder into my turn-in repository, there was a little trouble with save_path recognizing the Bear folder. Though you will editing the save path to a location of your choice, it is worth it to note this is being finnicky even though the other stations are done the exact same and working as expected. 


3. Testing using Baldrick 

Testing using this station's data showed consistency with aspects from both previous stations. The lack of column 17 and 18 were consistent with Bear Peninsula, but the naming of the columns was not an issue so the original code from Byrd worked with this station. 


Utilization of Dataset Differences 

Using noted dataset differences from running each separately, I am compiling a list of elements that will then be used in modifying the already started package function to encompass the elements in various stations so all can be run using the same function seamlessly in the future. 



PACKAGING THE CODE 


climate_sheets.ipynb 

Provided in the main project folder, this .ipynb file is the function intended to run the climatology data and produce the output products noted above. Currently, this function only works with Byrd station climatology sheet but will be the template for revision needed from differing elements from individual station tests in separate files as I did with the ones listed above. 

The function is called at the very bottom of the code cell using the parameters listed. The station_input should remain the same as what is listed as that is where the datafile is in this project folder, the name can stay the same as well. Please feel free to change the save_path as needed to ensure the saving of the products works to your desired location. 


climage_sheets folder 

This folder is the beginnings of the package that will be used in the future to execute the function in the climate_sheets.ipynb file in a python environment for those wanting access to data and products within the climatology .xlsx files provided and maintined by the AMRDC. 

The .py file is the same as the function in the climate_sheets.ipynb file along with the associated package necessities to start off with. 

Though the package script is not close to being ready for full operation, it highlights an important aspect regarding quality control and consistency of the data maintained by AMRDC that can be improved in the future. This also shows the need for packages to be flexible to varying datasets on the developers end. 