"""
A python module for executing maximmum and minimum value output and graphical products of mean temperature, wind, and pressure from the AMRDC climatology sheets.
"""

def climate_sheets(station_input, station_name, save_path):
    import pandas as pd
    import numpy as np
    import matplotlib.pyplot as plt
    import datetime
    
    """ 
    PARAMETERS
    ----------
    station_input : A .csv file for the climatology sheet data being read in 
    station_name: The name of the station being analyzed in the form of a string
    save_path: The location which you want your final output returns to be saved in the form of a string.
    
    RETURNS
    ----------
    mean_temp: a subplot graph set of the mean temperature values for each month of the stations history
    mean_wind: a subplot graph set of the mean wind speed values for each month of the stations history
    mean_pressure: a subplot graph set of the mean air pressure values for each month of the stations history
    max_min_values: a .csv file of the max and/or min values for temperature, wind speed, and air pressure over the stations history 
    """
    
    #Reading in .csv file
    df = pd.read_csv(station_input)
    
    #Rename columns to associate with data type 
    df = df.rename(columns={'Unnamed: 0':'month','Mean':'mean temp', '% of': '% temp missing', 'Unnamed: 3': 'max air temp', 
                             'Unnamed: 4': 'min air temp', 'Mean.1': 'mean wind speed (m/s)', '% of.1': '% wind missing', 'Unnamed: 7': 
                             'result wind direction', 'Unnamed: 8': 'result wind vv', 'Unnamed: 9': 'Con', 'Unnamed: 10': 'max wind direction',
                            'Unnamed: 11': 'max wind vv', 'Mean.2': 'mean air pressure (mb)', '% of.2': '% pressure missing', 'Unnamed: 14':
                            'max air pressure', 'Unnamed: 15': 'min air pressure', 'Unnamed: 16': 'theta (K)'})
    #Drop unnamed columns at the end with no data
    df = df.drop(columns = {'Unnamed: 17', 'Unnamed: 18'})
    
    #Drop the top four indecies now that everything is named
    df = df.drop(index = [0, 1, 2, 3])
    
    #Set the datatype as a string for the month column as it is in string format
    df['month'] = df['month'].astype('string')
    
    #Drop the rows in the column month that start with the following:
    df = df.loc[(df['month'] != 'MEAN') & (df['month'] != 'NaN') & (df['month'] != 'Month')]
    
    #Reset the indecies
    df.reset_index(inplace=True, drop=True)
    
    #Change the type of the column to numerics
    df['% pressure missing'] = pd.to_numeric(df['% pressure missing'], errors = 'coerce')
    
    #Create a new column in our dataframe for the year values
    df['year'] = ''
    
    #Create starting point for the years we are searching for
    start = df['% pressure missing'].loc[df['% pressure missing'] >= 1000]

    first_year = start[0]
    current_year = first_year

    start_index = df['% pressure missing'].loc[df['% pressure missing'] == first_year].index[0]
    end_index = 99999

    #Create for loop which will look for the years within the dataset column and assign values to the year column in the appropraite indecies 
    for val in (df['% pressure missing']): 

        if ((val == current_year) | (val < 1000) | (str(val) == 'nan')):
            continue
        else:
            end_index = df['% pressure missing'].loc[df['% pressure missing'] == val].index[0]-1
            df.loc[start_index : end_index, 'year'] = current_year
            start_index = end_index+1
            current_year = val
            if (val == 2022): 
                df.loc[end_index+2:, 'year'] = current_year

    #Change the data in the years column to numerics 
    df['year'] = pd.to_numeric(df['year'], errors = 'coerce')
    #Change values of mean temperature to float
    df['mean temp'] = df['mean temp'].astype('float')
    
    #Get rid of the month index lines that still contain string inputs 
    df = df.loc[df['month'] != 'Byrd (8903)']
    df = df.loc[df['month'] != 'Mean']
    df = df.loc[df['month'] != 'Byrd (08903)']
    
    #Make the numberic type for years into an integer 
    df['year'] = df['year'].astype('int')
    
    #Use the months and years to make a datetime column and add it to the dataset
    dates = pd.to_datetime(df['month'].astype('str') + df['year'].astype('str'), format='%b%Y')
    df['datetime'] = dates.dt.date

    #Set datettime as an index
    df = df.set_index(df['datetime'])
    
    #Create a for loop that will perform the above expanded graphing procedure 
    fig, ax1 = plt.subplots(3, 4, figsize = (20,15))
    axs = ax1.flatten()
    fig.set_facecolor('white')

    months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']

    x = 0
    y = 0

    for month in months:
        temp_mean = df.loc[df['month'] == month]
        ax1[x,y].plot(temp_mean['mean temp'])
        ax1[x,y].set_title(month + ' Mean Temperatures')
        ax1[x,y].set_xlabel('Year')
        ax1[x,y].set_ylabel('Temperature (Degrees C)')
    
        y = y + 1
        if y == 4:
            x = x + 1
            y = 0
        
    plt.tight_layout()
    plt.show()
    fig.savefig(save_path + '/' + station_name + '_mean_temp.png') #Add save_path/
    
    #Convert max air temperature to float type 
    df['max air temp'] = df['max air temp'].astype('float')
    
    #Convert min air temperature to float type 
    df['min air temp'] = df['min air temp'].astype('float')
    
    #Convert mean wind speed to float type 
    df['mean wind speed (m/s)'] = df['mean wind speed (m/s)'].astype('float')
    
    #Create a for loop that will perform the above expanded graphing procedure 
    fig, ax2 = plt.subplots(3, 4, figsize = (20,15))
    axs = ax2.flatten()
    fig.set_facecolor('white')

    months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']

    x = 0
    y = 0

    for month in months:
        wind_mean = df.loc[df['month'] == month]
        ax2[x,y].plot(wind_mean['mean wind speed (m/s)'])
        ax2[x,y].set_title(month + ' Mean Wind Speed')
        ax2[x,y].set_xlabel('Year')
        ax2[x,y].set_ylabel('Wind Speed (m/s)')
   
        y = y + 1
        if y == 4:
            x = x + 1
            y = 0
        
    plt.tight_layout()
    plt.show()
    fig.savefig(save_path + '/' + station_name + '_mean_wind.png')
    
    #Convert mean wind speed to float type 
    df['mean air pressure (mb)'] = df['mean air pressure (mb)'].astype('float')
    
    #Create a for loop that will perform the above expanded graphing procedure 
    fig, ax3 = plt.subplots(3, 4, figsize = (20,15))
    axs = ax3.flatten()
    fig.set_facecolor('white')

    months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']

    x = 0
    y = 0

    for month in months:
        p_mean = df.loc[df['month'] == month]
        ax3[x,y].plot(p_mean['mean air pressure (mb)'])
        ax3[x,y].set_title(month + ' Mean Air Pressure')
        ax3[x,y].set_xlabel('Year')
        ax3[x,y].set_ylabel('Pressure (mb)')
    
        y = y + 1
        if y == 4:
            x = x + 1
            y = 0
        
    plt.tight_layout()
    plt.show()
    fig.savefig(save_path + '/' + station_name + '_mean_pressure.png')
    
    df2 = df[['max air temp', 'min air temp', 'max wind vv', 'max air pressure', 'min air pressure']]
    print(df2)
    df2.to_csv(save_path + '/' + station_name + '_max_min_values.csv') 