setup(name = "climate_sheets",
    version = "0.0.1",
    author = "Anastasia Tomanek",
    packages=['climate_sheets'],
    install_requires=['numpy','matplotlib','pandas','datetime'])
